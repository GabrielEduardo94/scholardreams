import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class NotaVermelha here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class NotaVermelha extends Mover
{
    
    private int pontos = -10;
    
    /**
     * Act - do whatever the NotaVermelha wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        setLocation(getX(),getY()+1);

        if (atWorldEdge())
        {
            getWorld().removeObject(this);
        }
        else
        {
            Actor alun = getOneIntersectingObject(Aluno.class);
            
            if (alun != null)
            {
                MyWorld world = (MyWorld)getWorld();
                world.passaNota(pontos);
                world.removeObject(this);
            }
        }
    }    
}
