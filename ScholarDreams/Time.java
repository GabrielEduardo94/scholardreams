import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class Time here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Time extends Actor
{
     /**
     * Act - do whatever the Score wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    
    private int tempo = 3600;
    private int count = 0;
    private boolean controleTempo = false;
    
    public Time()
    {               
        act();        
    }  
    
    public void act() 
    {            
        
        if (count == 0)
        {
            GreenfootImage image = new GreenfootImage(" " + tempo/60,          
                                    30,
                                   new Color(0,0,0,200),
                                   new Color(255,255,255,0)); 
                                            
            setImage(image);            
        }
        
        if (tempo > 0){
             tempo--;
             if(tempo == 0) controleTempo = true;
        }
        
        // Redraws the score only at every 10 acts (saves CPU)
        if (count == 10)        
            count=0;
        else{
            count++;
        }
    }    
    
    public boolean getControleTempo(){
        return controleTempo;
    }
  
}
