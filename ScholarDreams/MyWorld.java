import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class MyWorld here.
 * 
 * @author Gabriel Lima, Natalia Vieira, Rafael Lima 
 * @version 0.2
 */
public class MyWorld extends greenfoot.World
{
    // Gives a 10% chance that a burguer may spawn in a given act
    private int taxaCriacaoNota = 10;    
    
    // Controles the number of acts in which burgers are allowed to spawn
    private int contadorCriacaoNota = 0;
    private int limiteCriacaoNota = 100;
    
    private Aluno aluno;
    private Score scr;
    private Time timer;
    private Score finalScore;
    private MenuButton button;
    private GameOver gameOver;
    
    // Game state
    private enum GameState{MENU,JOGANDO,GAMEOVER};    
    private GameState modo;
    
    
    /**
     * Constructor for objects of class MyWorld.
     * 
     */
    public MyWorld()
    {    
        // Create a new world with 600x400 cells with a cell size of 1x1 pixels.
        super(800, 600, 1); 
        
        comecaJogo();
        
    }
    
    public void act(){
  
        switch(modo)
        {
            case MENU:
            {
                actMenu();
                break;
            }            
            case JOGANDO:
            {                  
                actPlaying();   
                break;
            }            
            case GAMEOVER:
            {
                actGameOver();
                break;
            }
        }
    }
    
    //   ####################################### 
    // * ## Metodos de preparacao do mundo.   ## 
    // * ####################################### 
    
     private void comecaJogo(){
        // Clear the world
        removeObjects(getObjects(Actor.class));
        
        button = new MenuButton();        
        addObject(button,getWidth()-60,getHeight()-60);
        
        // Set the game state
        modo = GameState.MENU;
        }
     
     private void preparaLayout(){
        
         // Clear the world
        removeObjects(getObjects(Actor.class));
         
        scr = new Score();
        addObject(scr,100, 20);
        aluno = new Aluno();
        addObject(aluno, getWidth()/2, getHeight()-30);
        timer = new Time();
        addObject(timer, getWidth()/2, 20);
        
        
        modo = GameState.JOGANDO;
        }
        
        private void prepareGameOver()
    {
        // Clear the world
        removeObjects(getObjects(Actor.class));
        
        gameOver = new GameOver();
        addObject(gameOver,getWidth()-60,getHeight()-60);
                                
        // Set the game state
        modo = GameState.GAMEOVER;           
    }
        
    //   ####################################### 
    // * ## Metodos utilizaveis pelos atores. ## 
    // * #######################################
    
    public void passaNota(int pontos){
        scr.addScore(pontos);
    }
    
    
    //   ####################################### 
    // * ## Metodos das acoes do mundo.       ## 
    // * #######################################
    
    public void actMenu()
    {
        if(Greenfoot.mouseClicked(button))
        {
            preparaLayout();
        }
    }
    
        public void actPlaying()
    {
        
        int random = Greenfoot.getRandomNumber(100) + 1;
        
        
        if (contadorCriacaoNota == 0)
        {
            // Gives a burgerSpawnRatio% chance of spawning - increased by 2.5% at every 50 points
            //if (random < taxaCriacaoNota + (scr.getScore()/50)*2.5)
            if (random >= 50)
            {
                NotaAzul blue = new NotaAzul();
                int y = 20;
                int x = Greenfoot.getRandomNumber(getWidth()-39)+20; // Prevents the burger from spawning at extreme widths
                addObject(blue,x,y);
            }else{
                NotaVermelha red = new NotaVermelha();
                int y = 20;
                int x = Greenfoot.getRandomNumber(getWidth()-39)+20; // Prevents the burger from spawning at extreme widths
                addObject(red,x,y);
            }
        }
        
        // Prevents burgers from spawing every single act
        if (contadorCriacaoNota++ == limiteCriacaoNota)
        {
            contadorCriacaoNota=0;
        }
        
        if (timer.getControleTempo())
            prepareGameOver();
    }
    
    public void actGameOver()
    {
        if(Greenfoot.mouseClicked(gameOver) ||
           Greenfoot.mouseClicked(finalScore))
        {
            comecaJogo();
        }
    }
}
