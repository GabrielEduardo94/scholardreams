import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class Aluno here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Aluno extends Actor
{
    boolean moveNow;
    /**
     * Act - do whatever the Aluno wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        this.moveNow = false;
        int deltax = 0;
        int deltay = 0;
        int speed = 3;
        if (Greenfoot.isKeyDown("left")) {
            deltax = -speed;
            deltay = 0;
            this.moveNow = true;
        } else if (Greenfoot.isKeyDown("right")) {
            deltax = speed;
            deltay = 0;
            this.moveNow = true;
        }
        
        if (this.moveNow) {
            this.setLocation(this.getX() + deltax, this.getY() + deltay);
            
        }
    }    
}
